<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Relatório de Vendas</h1>
            <hr>
            <form id="form-report-sales" method="get">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="user_id">Cliente:</label>
                            <select name="user_id" id="user_id" class="form-control">
                                <option value="">Todos</option>
                                <?php foreach ($users as $user): ?>
                                    <option value="<?=$user->id?>"><?=$user->name?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="start">Início:</label>
                                    <input type="date" name="start" id="start" class="form-control" />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="end">Fim:</label>
                                    <input type="date" name="end" id="end" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status">Status:</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Todos</option>
                                <option value="0">Aguardando</option>
                                <option value="1">Pago</option>
                                <option value="2">Cancelado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="order">Ordernar:</label>
                            <select name="order" id="order" class="form-control">
                                <option value="desc">Mais recentes</option>
                                <option value="asc">Mais antigos</option>
                                <option value="status">Status</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center mt-5">
                        <input type="submit" class="btn btn-primary" value="Gerar Relatório" name="btn-report" id="btn-report" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>