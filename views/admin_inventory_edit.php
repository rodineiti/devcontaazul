<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/{$redirect}/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"])): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Criado sucesso.
                </div>
            <?php endif; ?>
            <h1>Editar produto</h1>
            <form method="POST" action="<?= BASE_URL?>admin/<?=$redirect?>/update/<?= $model->id; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?=$model->name?>" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control"><?=$model->description?></textarea>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="stock">Quantidade:</label>
                            <input type="number" name="stock" id="stock" class="form-control" value="<?=$model->stock?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="min_stock">Quantidade Mínima:</label>
                            <input type="number" name="min_stock" id="min_stock" class="form-control" value="<?=$model->min_stock?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="price">Preço (de):</label>
                            <input type="text" name="price" id="price" class="form-control" value="<?=str_price($model->price)?>" required />
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <input type="submit" value="Editar Produto" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>