<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/users/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "exists"): ?>
                <div class="alert alert-warning">
                    Este cliente já existe!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"])): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Criado sucesso.
                </div>
            <?php endif; ?>
            <h1>Adicionar cliente</h1>
            <form method="POST" action="<?= BASE_URL?>admin/users/store" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input type="email" name="email" id="email" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="password">Senha:</label>
                    <input type="password" name="password" id="password" class="form-control" required />
                </div>
                <h3>Informações de Endereço</h3>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" id="cep" required />
                </div>
                <div class="form-group">
                    <label for="address">Logradouro</label>
                    <input type="text" class="form-control" name="address" id="address" required />
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" id="number" required />
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" id="complement" />
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" id="district" required />
                </div>
                <div class="form-group">
                    <label for="city">Cidade</label>
                    <input type="text" class="form-control" name="city" id="city" required />
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <input type="text" class="form-control" name="state" id="state" required />
                </div>
                <div class="form-group">
                    <label for="country">País</label>
                    <input type="text" class="form-control" name="country" id="country" required />
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="number" maxlength="8" class="form-control" name="phone" id="phone" required />
                </div>
                <div class="form-group">
                    <label for="stars">Avaliação:</label>
                    <select name="stars" id="stars" class="form-control" required>
                        <option value="">Selecione</option>
                        <?php for ($i = 1; $i <= 5; $i++): ?>
                            <option value="<?=$i?>"><?=$i?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>