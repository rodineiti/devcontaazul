<style>
    th {
        text-align: left;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Relatório de Estoque</h1>
            <table class="table" border="1" width="100%">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Preço</th>
                    <th scope="col">Quantidade</th>
                    <th scope="col">Quantidade Mínima</th>
                    <th scope="col">Diferença</th>
                    <th scope="col">Criado em</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $item): ?>
                    <tr>
                        <th scope="row"><?= $item->id ?></th>
                        <td><?= $item->name ?></td>
                        <td><?= str_price($item->price) ?></td>
                        <td><?= $item->stock ?></td>
                        <td><?= $item->min_stock ?></td>
                        <td><?= ($item->min_stock - $item->stock) ?></td>
                        <td><?= $item->created_at ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>