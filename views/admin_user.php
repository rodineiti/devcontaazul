<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Lista de Clientes</h1>
            <hr>
            <?php if (hasPermission("users-create")): ?>
                <div class="row">
                    <div class="col">
                        <a href="<?= BASE_URL . "admin/{$redirect}/create"; ?>" class="btn btn-primary mb-2">Adicionar</a>
                    </div>
                    <div class="col">
                        <input type="text" id="search" name="search"
                               placeholder="Procurar" class="form-control" autocomplete="off" data-action="get_clients" />
                    </div>
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["error"])): ?>
                <div class="alert alert-danger">
                    Opss. Ocorreu um erro no processamento, tente mais tarde.
                </div>
            <?php endif; ?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Avaliação</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <th scope="row"><?= $user->id ?></th>
                        <td><?= $user->name ?></td>
                        <td><?= $user->email ?></td>
                        <td>
                            <?php if ($user->stars != "0"): ?>
                                <?php for ($i = 0; $i < intval($user->stars); $i++): ?>
                                    <img src="<?=image("star.png")?>" alt="star" height="20" />
                                <?php endfor; ?>
                            <?php endif; ?>
                        </td>
                        <td><?= $user->created_at ?></td>
                        <td>
                            <?php if (hasPermission("{$prefix}-edit")): ?>
                                <a href="<?= BASE_URL . "admin/{$redirect}/edit/" . $user->id; ?>" class="btn btn-info">Editar</a>
                            <?php endif; ?>
                            <?php if (hasPermission("{$prefix}-destroy")): ?>
                                <a href="<?= BASE_URL . "admin/{$redirect}/destroy/" . $user->id; ?>" class="btn btn-danger">Deletar</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <nav aria-label="...">
                <ul class="pagination">
                    <?php for($i = 1; $i <= $pages; $i++): ?>
                        <li class="page-item <?=($page === $i) ? "active" : ""?>">
                            <a class="page-link" href="<?= BASE_URL . "admin/{$redirect}/index" ?>?<?php
                            $pageArray["page"] = $i;
                            echo http_build_query($pageArray);
                            ?>"><?=$i?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>