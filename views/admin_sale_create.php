<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/{$redirect}/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"])): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Criado sucesso.
                </div>
            <?php endif; ?>
            <h1>Adicionar venda</h1>
            <form method="POST" action="<?= BASE_URL?>admin/<?=$redirect?>/store" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="user_id">Cliente:</label>
                    <select name="user_id" id="user_id" class="form-control" required>
                        <option value="">Selecione</option>
                        <?php foreach ($users as $user): ?>
                            <option value="<?=$user->id?>"><?=$user->name?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status:</label>
                    <select name="status" id="status" class="form-control" required>
                        <option value="">Selecione</option>
                        <option value="0">Aguardando</option>
                        <option value="1">Pago</option>
                        <option value="2">Cancelado</option>
                    </select>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-12 age-range-container">
                        <h3>Produtos</h3>
                        <div class="row age-range-inputs">
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label" for="product_id">Produto <span class="required">*</span>
                                    </label>
                                    <select class="form-control product_id" name="product_id[]" required>
                                        <option value="">Selecione</option>
                                        <?php foreach ($products as $product): ?>
                                            <option value="<?=$product->id?>"><?=$product->name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label" for="qty">Quantidade <span class="required">*</span>
                                    </label>
                                    <input type="number" class="form-control qty" name="qty[]" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label" for="sale_price">Valor <span class="required">*</span>
                                    </label>
                                    <input type="text" class="form-control sale_price" name="sale_price[]" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label">&nbsp;</label>
                                    <button type="button" class="btn btn-danger remove-age-range" style="margin-top:25px;">Deletar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-warning add-age-range">+ Adicionar produto</a>
                    </div>
                </div>

                <hr>

                <div class="form-group text-right">
                    <input type="submit" value="Criar Venda" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>