<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-4">
                    <div class="card text-center">
                        <div class="card-header">Clientes</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_users?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card text-center">
                        <div class="card-header">Grupos</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_groups?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card text-center">
                        <div class="card-header">Permissões</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_items?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card text-center">
                        <div class="card-header">Estoque</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_inventories?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card text-center">
                        <div class="card-header">Vendas</div>
                        <div class="card-body">
                            <h5 class="card-title">R$ <?=str_price($sum_sales ?? 0)?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card text-center">
                        <div class="card-header">Compras</div>
                        <div class="card-body">
                            <h5 class="card-title">R$ <?=str_price($sum_purchases)?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">Despesas e Receitas dos últimos 30 dias</div>
                        <div class="card-body">
                            <canvas id="report-resume"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-header">Status de Pagamento</div>
                        <div class="card-body">
                            <canvas id="report-status" height="330"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<script src="<?=asset("js/admin/Chart.min.js")?>"></script>
<script>
    reportResume();
    reportStatus();
    function reportResume() {
        var content = document.getElementById("report-resume").getContext("2d");
        var chart = new Chart(content, {
            type:'line',
            data: {
                labels: <?=json_encode($days_list)?>,
                datasets: [{
                    label:"Vendas",
                    backgroundColor:"#0000FF",
                    borderColor:"#0000FF",
                    data:<?=json_encode(array_values($report_sales))?>,
                    fill:false
                }, {
                    label:"Compras",
                    backgroundColor:"#FF0000",
                    borderColor:"#FF0000",
                    data:<?=json_encode(array_values($report_purchages))?>,
                    fill:false
                }]
            }
        });
    }
    function reportStatus() {
        var content = document.getElementById("report-status").getContext("2d");
        var chart = new Chart(content, {
            type:'pie',
            data: {
                labels: ["Aguardando Pagto","Pago","Cancelado"],
                datasets: [{
                    backgroundColor:["#FFCE56","#36A2EB","#FF6384"],
                    data:<?=json_encode(array_values($report_status))?>,
                }]
            }
        });
    }
</script>