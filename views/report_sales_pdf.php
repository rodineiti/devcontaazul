<style>
    th {
        text-align: left;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Relatório de Vendas</h1>
            <fieldset>
                <?php if (isset($filters["start"]) && isset($filters["end"])): ?>
                <p>Período: <?=date("d/m/Y", strtotime($filters["start"]))?> à <?=date("d/m/Y", strtotime($filters["end"]))?></p>
                <?php endif; ?>
                <?php if (isset($filters["status"])): ?>
                    <p>Status: <?=setStatus($filters["status"])?></p>
                <?php endif; ?>
            </fieldset>
            <table class="table" border="1" width="100%">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Status</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Criado em</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $item): ?>
                    <tr>
                        <th scope="row"><?= $item->id ?></th>
                        <td><?= $item->name ?></td>
                        <td><?= $item->status ?></td>
                        <td><?= str_price($item->amount) ?></td>
                        <td><?= $item->created_at ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>