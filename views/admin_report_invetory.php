<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Relatório de Estoque</h1>
            <hr>
            <form id="form-report-inventories" method="get">
                <div class="row">
                    <div class="col-sm-12 text-center mt-5">
                        <input type="submit" class="btn btn-primary" value="Gerar Relatório" name="btn-report" id="btn-report" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>