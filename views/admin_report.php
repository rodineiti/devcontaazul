<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Relatórios</h1>
            <hr>
            <div class="row">
                <div class="col-md-4 p-2">
                    <div class="card text-center">
                        <div class="card-header">Vendas</div>
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="<?= BASE_URL . "admin/reports/sales"?>"
                                   class="btn btn-primary">Acessar</a>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-2">
                    <div class="card text-center">
                        <div class="card-header">Estoque</div>
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="<?= BASE_URL . "admin/reports/inventories"?>"
                                   class="btn btn-primary">Acessar</a>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>