<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/{$redirect}/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"])): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Criado sucesso.
                </div>
            <?php endif; ?>
            <h1>Editar compra</h1>
            <form method="POST" action="<?= BASE_URL?>admin/<?=$redirect?>/update/<?= $model->id; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="amount">Valor Total:</label>
                    <input type="text" name="amount" id="amount" disabled value="<?= str_price($model->amount); ?>" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="status">Status:</label>
                    <select name="status" id="status" class="form-control" required>
                        <option value="">Selecione</option>
                        <option value="0" <?=selected($model->status == "0")?>>Aguardando</option>
                        <option value="1" <?=selected($model->status == "1")?>>Pago</option>
                        <option value="2" <?=selected($model->status == "2")?>>Cancelado</option>
                    </select>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-12 age-range-container">
                        <h3>Produtos</h3>
                        <?php foreach ($purchaseProducts as $purchaseProduct): ?>
                        <div class="row age-range-inputs">
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label" for="name">Produto <span class="required">*</span>
                                    </label>
                                    <input type="text" class="form-control name" name="name[]" value="<?=$purchaseProduct->name?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label" for="qty">Quantidade <span class="required">*</span>
                                    </label>
                                    <input type="number" class="form-control qty" name="qty[]" value="<?=$purchaseProduct->qty?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label" for="purchase_price">Valor <span class="required">*</span>
                                    </label>
                                    <input type="text" class="form-control purchase_price" name="purchase_price[]" value="<?=str_price($purchaseProduct->purchase_price)?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <label class="control-label">&nbsp;</label>
                                    <button type="button" class="btn btn-danger remove-age-range" style="margin-top:25px;">Deletar</button>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-warning add-age-range">+ Adicionar produto</a>
                    </div>
                </div>

                <hr>

                <div class="form-group text-right">
                    <input type="submit" value="Editar Compra" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>