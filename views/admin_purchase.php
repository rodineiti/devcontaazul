<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Lista de Compras</h1>
            <hr>
            <?php if (hasPermission("{$prefix}-create")): ?>
                <div class="row">
                    <div class="col">
                        <a href="<?= BASE_URL . "admin/{$redirect}/create"; ?>" class="btn btn-primary mb-2">Adicionar</a>
                    </div>
                    <div class="col">

                    </div>
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["error"])): ?>
                <div class="alert alert-danger">
                    Opss. Ocorreu um erro no processamento, tente mais tarde.
                </div>
            <?php endif; ?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Status</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $item): ?>
                    <tr>
                        <th scope="row"><?= $item->id ?></th>
                        <td><?= $item->status ?></td>
                        <td><?= str_price($item->amount) ?></td>
                        <td><?= $item->created_at ?></td>
                        <td>
                            <?php if (hasPermission("{$prefix}-edit")): ?>
                                <a href="<?= BASE_URL . "admin/{$redirect}/edit/" . $item->id; ?>" class="btn btn-info">Editar</a>
                            <?php endif; ?>
                            <?php if (hasPermission("{$prefix}-destroy")): ?>
                                <a href="<?= BASE_URL . "admin/{$redirect}/destroy/" . $item->id; ?>" class="btn btn-danger">Deletar</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <nav aria-label="...">
                <ul class="pagination">
                    <?php for($i = 1; $i <= $pages; $i++): ?>
                        <li class="page-item <?=($page === $i) ? "active" : ""?>">
                            <a class="page-link" href="<?= BASE_URL . "admin/{$redirect}/index" ?>?<?php
                            $pageArray["page"] = $i;
                            echo http_build_query($pageArray);
                            ?>"><?=$i?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>