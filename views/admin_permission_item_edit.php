<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/{$redirect}/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"]) && $_GET["success"] === "edit"): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Atualizado sucesso.
                </div>
            <?php endif; ?>
            <h1>Editar item de permissão</h1>
            <form method="POST" action="<?= BASE_URL?>admin/<?=$redirect?>/update/<?= $itemPermission->id; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" value="<?= $itemPermission->name?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" id="slug" class="form-control" value="<?= $itemPermission->slug?>" disabled required />
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>