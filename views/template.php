<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">
    <link href="<?=asset("css/bootstrap.min.css")?>" rel="stylesheet" id="bootstrap-css">
    <link href="<?=asset("css/jquery-ui.min.css") ?>" rel="stylesheet" id="jquery-ui-css">
    <link rel="stylesheet" href="<?=asset("css/style.css")?>">

    <script src="<?=asset("js/jquery.min.js")?>"></script>
</head>
<body>

<nav class="navbar topnav">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo BASE_URL; ?>">Home</a></li>
            <li><a href="#">Contato</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php if(auth()): ?>
                <li class="dropdown">
                    <a class="nav-item nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:;"><?=auth()->name?></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/profile"><?=auth()->name?></a></li>
                        <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/logout">Sair</a></li>
                    </ul>
                </li>
            <?php else: ?>
                <li><a href="<?php echo BASE_URL; ?>auth?login">Login</a></li>
                <li><a href="<?php echo BASE_URL; ?>auth/register">Cadastro</a></li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-sm-12"><?php $this->viewTemplate($view, $data); ?></div>
    </div>
</div>
<footer>
    Footer
</footer>

<script>
    var baseUrl = '<?=BASE_URL?>';
</script>
<script src="<?=asset("js/bootstrap.min.js")?>"></script>
<script src="<?=asset("js/jquery-ui.min.js")?>"></script>
<script src="<?=asset("js/script.js")?>"></script>
</body>
</html>