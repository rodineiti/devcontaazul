<div class="card">
    <div class="card-body">
        <div class="nav flex-column nav-pills" aria-orientation="vertical">
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/home"])?>" href="<?= BASE_URL . "admin/home";?>">
                Home
            </a>
            <?php if (hasPermission("users-index")): ?>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/users/index","admin/users/create","admin/users/edit"])?>" href="<?= BASE_URL . "admin/users/index";?>">
                Clientes
            </a>
            <?php endif; ?>
            <?php if (hasPermission("permission-groups-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/permissiongroups/index","admin/permissiongroups/create","admin/permissiongroups/edit"])?>" href="<?= BASE_URL . "admin/permissiongroups/index";?>">
                    Grupo de Permissões
                </a>
            <?php endif; ?>
            <?php if (hasPermission("permission-items-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/permissionitems/index","admin/permissionitems/create","admin/permissionitems/edit"])?>" href="<?= BASE_URL . "admin/permissionitems/index";?>">
                    Itens de Permissões
                </a>
            <?php endif; ?>
            <?php if (hasPermission("inventories-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/inventories/index","admin/inventories/create","admin/inventories/edit"])?>" href="<?= BASE_URL . "admin/inventories/index";?>">
                    Estoque
                </a>
            <?php endif; ?>
            <?php if (hasPermission("sales-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/sales/index","admin/sales/create","admin/sales/edit"])?>" href="<?= BASE_URL . "admin/sales/index";?>">
                    Vendas
                </a>
            <?php endif; ?>
            <?php if (hasPermission("purchases-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/purchases/index","admin/purchases/create","admin/purchases/edit"])?>" href="<?= BASE_URL . "admin/purchases/index";?>">
                    Compras
                </a>
            <?php endif; ?>
            <?php if (hasPermission("reports-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/reports/index","admin/reports/create","admin/reports/edit"])?>" href="<?= BASE_URL . "admin/reports/index";?>">
                    Relatórios
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>