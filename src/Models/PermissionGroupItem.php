<?php

namespace Src\Models;

use Src\Core\Model;

class PermissionGroupItem extends Model
{
    public function __construct()
    {
        parent::__construct("permission_group_items");
    }

    public function getByPermission($permission_group_id, $company_id)
    {
        $permissions = $this->readJoin(
            true,
            ["permission_items.slug"],
            ["permission_group_items.permission_group_id" => $permission_group_id, "permission_group_items.company_id" => $company_id],
            ["INNER JOIN permission_items", "permission_items.id = permission_group_items.permission_item_id"]) ?? [];

        $data = [];

        foreach ($permissions as $item) {
            $data[] = $item->slug;
        }

        return $data;
    }

    public function getByPermissionItem($permission_item_id)
    {
        $permissions = $this->readJoin(
                true,
                ["permission_group_items.id"],
                ["permission_group_items.permission_item_id" => $permission_item_id, "permission_group_items.company_id" => auth("admins")->company_id],
                ["INNER JOIN permission_items", "permission_items.id = permission_group_items.permission_item_id"]) ?? [];

        return $permissions;
    }

    public function checkPermission($permission_group_id, $permission_item_id)
    {
        $check = $this->readJoin(
                false,
                ["permission_items.id"],
                ["permission_group_items.permission_group_id" => $permission_group_id,
                    "permission_group_items.permission_item_id" => $permission_item_id,
                    "permission_group_items.company_id" => auth("admins")->company_id],
                ["INNER JOIN permission_items", "permission_items.id = permission_group_items.permission_item_id"]) ?? [];

        return ($check) ? true : false;
    }

    public function destroyAllByPermission($permission_id)
    {
        $permissionItems = $this->read(true, ["*"], ["permission_group_id" => $permission_id, "company_id" => auth("admins")->company_id]);
        foreach ($permissionItems as $item) {
            $this->delete(["id" => $item->id]);
        }
    }

    public function destroyAllByPermissionItem($item_id)
    {
        $permissionItems = $this->read(true, ["*"], ["permission_item_id" => $item_id, "company_id" => auth("admins")->company_id]);
        foreach ($permissionItems as $item) {
            $this->delete(["id" => $item->id]);
        }
    }
}

?>