<?php

namespace Src\Models;

use Src\Core\Model;

class Purchase extends Model
{
    public function __construct()
    {
        parent::__construct("purchases");
    }

    public function all($filters = [], $limit = null, $offset = null)
    {
        $query = "SELECT * FROM purchases ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        if (isset($filters["order"]) && !empty($filters["order"])) {
            switch ($filters["order"]) {
                case "desc":
                    $query .= " ORDER BY purchases.created_at DESC ";
                    break;
                case "asc":
                    $query .= " ORDER BY purchases.created_at ASC ";
                    break;
                case "status":
                    $query .= " ORDER BY purchases.status ASC ";
                    break;
            }
        } else {
            $query .= " ORDER BY purchases.created_at DESC ";
        }

        if ($limit) {
            $query .= " LIMIT {$limit} ";
        }

        if ($offset) {
            $query .= " OFFSET {$offset} ";
        }

        $results = $this->customQuery($query) ?? [];

        return $results;
    }

    public function sum($filters = [])
    {
        $query = "SELECT sum(purchases.amount) as total FROM purchases ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        $result = $this->customQuery($query, false) ?? null;

        return $result ? $result->total : 0;
    }

    public function resumeStatus($filters = [])
    {
        $query = "SELECT purchases.status, count(purchases.id) as total FROM purchases ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        $query .= " GROUP BY purchases.status ORDER BY purchases.status ASC ";

        $results = $this->customQuery($query) ?? [];

        $data = ["0" => 0, "1" => 0, "2" => 0];
        foreach ($results as $item) {
            $data[$item->status] = $item->total;
        }

        return $data;
    }

    public function resumeGrafic($filters = [])
    {
        $data = array();

        $currentDay = $filters["start"] ?? date("Y-m-d");

        while ($filters["end"] != $currentDay) {
            $data[$currentDay] = 0;
            $currentDay = date("Y-m-d", strtotime("+1 days", strtotime($currentDay)));
        }

        $query = "SELECT DATE_FORMAT(purchases.created_at, '%Y-%m-%d') as created_at, sum(purchases.amount) as total FROM purchases ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        $query .= " GROUP BY DATE_FORMAT(purchases.created_at, '%Y-%m-%d') ";

        $results = $this->customQuery($query) ?? [];

        foreach ($results as $item) {
            $data[$item->created_at] = $item->total;
        }

        return $data;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $data["company_id"] = auth("admins")->company_id;
        $data["admin_id"] = auth("admins")->id;
        $data["amount"] = 0;

        $dataProducts = $data;
        unset($data["name"]);
        unset($data["qty"]);
        unset($data["purchase_price"]);

        $modelId = $this->insert($data);

        if ($modelId) {
            $total = 0;
            if (isset($dataProducts["name"]) && is_array($dataProducts["name"])) {
                foreach($dataProducts["name"] as $key => $item) {
                    (new PurchasesProduct())->create([
                        "purchase_id" => $modelId,
                        "name" => $item,
                        "qty" => intval($dataProducts["qty"][$key]),
                        "purchase_price" => str_price_db($dataProducts["purchase_price"][$key]),
                    ]);
                    $total += (floatval($dataProducts["purchase_price"][$key]) * intval($dataProducts["qty"][$key]));
                }
            }

            $this->update(["amount" => str_price_db($total)], ["id" => $modelId]);
            return $this->getById($modelId);
        }

        return null;
    }

    public function updateData($id, array $data)
    {
        $dataProducts = $data;
        unset($data["name"]);
        unset($data["qty"]);
        unset($data["purchase_price"]);

        if (count($data)) {
            $data["company_id"] = auth("admins")->company_id;
            $data["admin_id"] = auth("admins")->id;

            $total = 0;
            if (isset($dataProducts["name"]) && is_array($dataProducts["name"])) {
                (new PurchasesProduct())->destroyAll($id);
                foreach($dataProducts["name"] as $key => $item) {
                    (new PurchasesProduct())->create([
                        "purchase_id" => $id,
                        "name" => $item,
                        "qty" => intval($dataProducts["qty"][$key]),
                        "purchase_price" => str_price_db($dataProducts["purchase_price"][$key]),
                    ]);

                    $total += (floatval($dataProducts["purchase_price"][$key]) * intval($dataProducts["qty"][$key]));
                }
            }

            $data["amount"] = str_price_db($total);

            if ($this->update($data, ["id" => $id])) {
                $model = $this->read(false, ["*"], ["id" => $id]);
                return $model;
            }
        }

        return false;
    }

    public function destroy($id)
    {
        (new PurchasesProduct())->destroyAll($id);
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }

    private function buildWhere($filters = [])
    {
        $where = [];

        $where[] = "purchases.company_id = " . auth("admins")->company_id;

        if (isset($filters["start"]) && !empty($filters["start"]) && isset($filters["end"]) && !empty($filters["end"])) {
            $where[] = "purchases.created_at BETWEEN '{$filters["start"]}' AND '{$filters["end"]}'";
        }

        if (isset($filters["status"]) && !empty($filters["status"])) {
            $where[] = "purchases.status = {$filters["status"]}";
        }

        return $where;
    }
}

?>