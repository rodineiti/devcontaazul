<?php

namespace Src\Models;

use Src\Core\Model;

class InventoryHistory extends Model
{
    public function __construct()
    {
        parent::__construct("inventory_history");
    }

    public function all($filters = [], $limit = 10, $offset = 0)
    {
        $query = "SELECT * FROM inventory_history ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        if ($limit) {
            $query .= " LIMIT {$limit} ";
        }

        if ($offset) {
            $query .= " OFFSET {$offset} ";
        }

        $results = $this->customQuery($query) ?? [];

        return $results;
    }

    public function create(array $data)
    {
        $data["company_id"] = auth("admins")->company_id;
        $data["admin_id"] = auth("admins")->id;

        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }

        return null;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if (count($data)) {
            $data["company_id"] = auth("admins")->company_id;
            if ($this->update($data, ["id" => $id])) {
                $model = $this->read(false, ["*"], ["id" => $id]);
                return $model;
            }
        }

        return false;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }

    private function buildWhere($filters = [])
    {
        $where = [];
        $where[] = "inventory_history.company_id = " . auth("admins")->company_id;
        return $where;
    }
}

?>