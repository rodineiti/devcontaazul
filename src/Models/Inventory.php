<?php

namespace Src\Models;

use Src\Core\Model;

class Inventory extends Model
{
    public function __construct()
    {
        parent::__construct("inventories");
    }

    public function all($filters = [], $limit = 10, $offset = 0)
    {
        $query = "SELECT *, (inventories.min_stock - inventories.stock) as diference FROM inventories ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        if (isset($filters["min"]) && !empty($filters["min"])) {
            $query .= " ORDER BY diference DESC ";
        }

        if ($limit) {
            $query .= " LIMIT {$limit} ";
        }

        if ($offset) {
            $query .= " OFFSET {$offset} ";
        }

        $results = $this->customQuery($query) ?? [];

        return $results;
    }

    public function create(array $data)
    {
        $data["company_id"] = auth("admins")->company_id;
        $data["price"] = str_price_db($data["price"]);

        $modelId = $this->insert($data);

        if ($modelId) {
            (new InventoryHistory())->create([
                "inventory_id" => $modelId,
                "action_type" => "add"
            ]);
            return $this->getById($modelId);
        }

        return null;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if (count($data)) {
            $data["company_id"] = auth("admins")->company_id;
            $data["price"] = str_price_db($data["price"]);
            if ($this->update($data, ["id" => $id])) {
                (new InventoryHistory())->create([
                    "inventory_id" => $id,
                    "action_type" => "edit"
                ]);
                $model = $this->read(false, ["*"], ["id" => $id]);
                return $model;
            }
        }

        return false;
    }

    public function decrease($id, array $data)
    {
        $model = $this->read(false, ["*"], ["id" => $id]);

        $stock = ($model->stock - $data["qty"]);
        $data["stock"] = $stock;
        unset($data["qty"]);

        if ($this->update($data, ["id" => $id])) {
            (new InventoryHistory())->create([
                "inventory_id" => $id,
                "action_type" => "down"
            ]);
            return $this->read(false, ["*"], ["id" => $id]);
        }

        return false;
    }

    public function destroy($id)
    {
        (new InventoryHistory())->create([
            "inventory_id" => $id,
            "action_type" => "del"
        ]);

        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }

    private function buildWhere($filters = [])
    {
        $where = [];

        $where[] = "inventories.company_id = " . auth("admins")->company_id;

        if (!empty($filters["searchTerm"])) {
            $where[] = "(inventories.name LIKE '%" . $filters["searchTerm"] . "%' OR inventories.description LIKE '%" . $filters["searchTerm"] . "%' OR inventories.id LIKE '%" . $filters["searchTerm"] . "%') ";
        }

        if (isset($filters["min"]) && !empty($filters["min"])) {
            $where[] = "inventories.stock <= inventories.min_stock";
        }

        return $where;
    }
}

?>