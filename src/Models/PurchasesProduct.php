<?php

namespace Src\Models;

use Src\Core\Model;

class PurchasesProduct extends Model
{
    public function __construct()
    {
        parent::__construct("purchases_products");
    }

    public function getByPurchase($purchase_id)
    {
        $products = $this->read(true, ["*"], ["purchase_id" => $purchase_id]) ?? [];
        return $products;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $data["company_id"] = auth("admins")->company_id;

        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }

        return null;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }

    public function destroyAll($purchase_id)
    {
        $products = $this->read(true, ["*"], ["purchase_id" => $purchase_id]);
        foreach ($products as $product) {
            $this->delete(["id" => $product->id]);
        }
        return true;
    }
}

?>