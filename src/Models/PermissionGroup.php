<?php

namespace Src\Models;

use Src\Core\Model;

class PermissionGroup extends Model
{
    public function __construct()
    {
        parent::__construct("permission_groups");
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function all($where = [])
    {
        $where["company_id"] = auth("admins")->company_id;
        $results = $this->read(true, ["*"], $where) ?? [];
        return $results;
    }

    public function create(array $data)
    {
        if (empty($data["name"])) {
            return false;
        }

        if (isset($data["permissionItems"]) && !is_array($data["permissionItems"])) {
            $data["permissionItems"] = array();
        }

        $modelId = $this->insert(["name" => $data["name"], "company_id" => auth("admins")->company_id]);

        if ($modelId) {
            foreach ($data["permissionItems"] as $item) {
                (new PermissionGroupItem())->insert([
                    "permission_group_id" => $modelId,
                    "permission_item_id" => $item,
                    "company_id" => auth("admins")->company_id
                ]);
            }
            return $this->getById($modelId);
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if (empty($data["name"])) {
            return false;
        }

        if (isset($data["permissionItems"]) && !is_array($data["permissionItems"])) {
            $data["permissionItems"] = array();
        }

        if ($this->update(["name" => $data["name"], "company_id" => auth("admins")->company_id], ["id" => $id])) {
            (new PermissionGroupItem())->destroyAllByPermission($id);
            foreach ($data["permissionItems"] as $item) {
                (new PermissionGroupItem())->insert([
                    "permission_group_id" => $id,
                    "permission_item_id" => $item,
                    "company_id" => auth("admins")->company_id
                ]);
            }
            return $this->getById($id);
        }
        return false;
    }

    public function destroy($id)
    {
        (new PermissionGroupItem())->destroyAllByPermission($id);
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }
}

?>