<?php

namespace Src\Models;

use Src\Core\Model;

class PermissionItem extends Model
{
    public function __construct()
    {
        parent::__construct("permission_items");
    }

    public function all($where = [])
    {
        $where["company_id"] = auth("admins")->company_id;
        $results = $this->read(true, ["*"], $where) ?? [];
        return $results;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        if (empty($data["name"])) {
            return false;
        }

        if (empty($data["slug"])) {
            return false;
        }

        $data["slug"] = str_slug($data["slug"]);
        $data["company_id"] = auth("admins")->company_id;

        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if (empty($data["name"])) {
            return false;
        }

        unset($data["slug"]);
        $data["company_id"] = auth("admins")->company_id;

        if ($this->update($data, ["id" => $id])) {
            return $this->getById($id);
        }
        return false;
    }

    public function destroy($id)
    {
        (new PermissionGroupItem())->destroyAllByPermissionItem($id);
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }
}

?>