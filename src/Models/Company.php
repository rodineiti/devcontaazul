<?php

namespace Src\Models;

use Src\Core\Model;

class Company extends Model
{
    public function __construct()
    {
        parent::__construct("companies");
    }

    public function all($where = [])
    {
        $results = $this->read(true, ["*"], $where) ?? [];
        return $results;
    }

    public function create(array $data)
    {
        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }

        return null;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if (!empty($data["name"])) {
            $newData["name"] = $data["name"];
        }

        if (count($newData)) {
            if ($this->update($newData, ["id" => $id])) {
                $model = $this->read(false, ["*"], ["id" => $id]);
                return $model;
            }
        }

        return false;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id]);
    }
}

?>