<?php

namespace Src\Models;

use Src\Core\Model;

class SaleProduct extends Model
{
    public function __construct()
    {
        parent::__construct("sales_products");
    }

    public function getBySale($sale_id)
    {
        $products = $this->read(true, ["*"], ["sale_id" => $sale_id]) ?? [];
        return $products;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $data["company_id"] = auth("admins")->company_id;

        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }

        return null;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }

    public function destroyAll($sale_id)
    {
        $products = $this->read(true, ["*"], ["sale_id" => $sale_id]);
        foreach ($products as $product) {
            $this->delete(["id" => $product->id]);
        }
        return true;
    }
}

?>