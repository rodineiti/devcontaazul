<?php

namespace Src\Models;

use Src\Core\Model;

class Sale extends Model
{
    public function __construct()
    {
        parent::__construct("sales");
    }

    public function all($filters = [], $limit = null, $offset = null)
    {
        $query = "SELECT * FROM sales ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        if (isset($filters["order"]) && !empty($filters["order"])) {
            switch ($filters["order"]) {
                case "desc":
                    $query .= " ORDER BY sales.created_at DESC ";
                    break;
                case "asc":
                    $query .= " ORDER BY sales.created_at ASC ";
                    break;
                case "status":
                    $query .= " ORDER BY sales.status ASC ";
                    break;
            }
        } else {
            $query .= " ORDER BY sales.created_at DESC ";
        }

        if ($limit) {
            $query .= " LIMIT {$limit} ";
        }

        if ($offset) {
            $query .= " OFFSET {$offset} ";
        }

        $results = $this->customQuery($query) ?? [];

        foreach ($results as $result) {
            $user = (new User())->getById($result->user_id);
            $result->name = $user->name;
        }

        return $results;
    }

    public function sum($filters = [])
    {
        $query = "SELECT sum(sales.amount) as total FROM sales ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        $result = $this->customQuery($query, false) ?? null;

        return $result ? $result->total : 0;
    }

    public function resumeStatus($filters = [])
    {
        $query = "SELECT sales.status, count(sales.id) as total FROM sales ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        $query .= " GROUP BY sales.status ORDER BY sales.status ASC ";

        $results = $this->customQuery($query) ?? [];

        $data = ["0" => 0, "1" => 0, "2" => 0];
        foreach ($results as $item) {
            $data[$item->status] = $item->total;
        }

        return $data;
    }

    public function resumeGrafic($filters = [])
    {
        $data = array();

        $currentDay = $filters["start"] ?? date("Y-m-d");

        while ($filters["end"] != $currentDay) {
            $data[$currentDay] = 0;
            $currentDay = date("Y-m-d", strtotime("+1 days", strtotime($currentDay)));
        }

        $query = "SELECT DATE_FORMAT(sales.created_at, '%Y-%m-%d') as created_at, sum(sales.amount) as total FROM sales ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        $query .= " GROUP BY DATE_FORMAT(sales.created_at, '%Y-%m-%d') ";

        $results = $this->customQuery($query) ?? [];

        foreach ($results as $item) {
            $data[$item->created_at] = $item->total;
        }

        return $data;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $data["company_id"] = auth("admins")->company_id;
        $data["admin_id"] = auth("admins")->id;
        $data["amount"] = 0;

        $dataProducts = $data;
        unset($data["product_id"]);
        unset($data["qty"]);
        unset($data["sale_price"]);

        $modelId = $this->insert($data);

        if ($modelId) {
            $total = 0;
            if (isset($dataProducts["product_id"]) && is_array($dataProducts["product_id"])) {
                foreach($dataProducts["product_id"] as $key => $item) {
                    (new SaleProduct())->create([
                        "sale_id" => $modelId,
                        "product_id" => $item,
                        "qty" => intval($dataProducts["qty"][$key]),
                        "sale_price" => str_price_db($dataProducts["sale_price"][$key]),
                    ]);

                    (new Inventory())->decrease($item, ["qty" => intval($dataProducts["qty"][$key])]);

                    $total += (floatval($dataProducts["sale_price"][$key]) * intval($dataProducts["qty"][$key]));
                }
            }

            $this->update(["amount" => str_price_db($total)], ["id" => $modelId]);
            return $this->getById($modelId);
        }

        return null;
    }

    public function updateData($id, array $data)
    {
        $dataProducts = $data;
        unset($data["product_id"]);
        unset($data["qty"]);
        unset($data["sale_price"]);

        if (count($data)) {
            $data["company_id"] = auth("admins")->company_id;
            $data["admin_id"] = auth("admins")->id;

            $total = 0;
            if (isset($dataProducts["product_id"]) && is_array($dataProducts["product_id"])) {
                (new SaleProduct())->destroyAll($id);
                foreach($dataProducts["product_id"] as $key => $item) {
                    (new SaleProduct())->create([
                        "sale_id" => $id,
                        "product_id" => $item,
                        "qty" => intval($dataProducts["qty"][$key]),
                        "sale_price" => str_price_db($dataProducts["sale_price"][$key]),
                    ]);

                    $total += (floatval($dataProducts["sale_price"][$key]) * intval($dataProducts["qty"][$key]));
                }
            }

            $data["amount"] = str_price_db($total);

            if ($this->update($data, ["id" => $id])) {
                $model = $this->read(false, ["*"], ["id" => $id]);
                return $model;
            }
        }

        return false;
    }

    public function destroy($id)
    {
        (new SaleProduct())->destroyAll($id);
        return $this->delete(["id" => $id, "company_id" => auth("admins")->company_id]);
    }

    private function buildWhere($filters = [])
    {
        $where = [];

        $where[] = "sales.company_id = " . auth("admins")->company_id;

        if (isset($filters["user_id"]) && !empty($filters["user_id"])) {
            $where[] = "sales.user_id = {$filters["user_id"]}";
        }

        if (isset($filters["start"]) && !empty($filters["start"]) && isset($filters["end"]) && !empty($filters["end"])) {
            $where[] = "sales.created_at BETWEEN '{$filters["start"]}' AND '{$filters["end"]}'";
        }

        if (isset($filters["status"]) && !empty($filters["status"])) {
            $where[] = "sales.status = {$filters["status"]}";
        }

        return $where;
    }
}

?>