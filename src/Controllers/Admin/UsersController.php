<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\User;

class UsersController extends Controller
{
    protected $user;
    protected $redirect = "users";
    protected $prefix = "users";
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->user = new User();
        $this->required = ["name","email","password"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $limit = $this->limit ?? 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $users = $this->user->all([], $limit, $offset);
        $usersCount = $this->user->count(["id"]);
        $pages = ceil($usersCount / $limit);

        $data = array();
        $data["users"] = $users;
        $data["total"] = count($users);
        $data["pages"] = $pages;
        $data["page"] = $page;
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_user", $data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $this->template("admin_user_create");
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $user = $this->user->create($data);

        if (!$user) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        setFlashMessage("success", ["Cliente adicionado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$user = $this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $data = array();
        $data["user"] = $user;
        $this->template("admin_user_edit", $data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        if (empty($data["name"]) || empty($data["email"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->user->updateProfile($id, $data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        setFlashMessage("success", ["Cliente atualizado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$user = $this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->user->destroy($user->id);

        setFlashMessage("success", ["Cliente deletado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}