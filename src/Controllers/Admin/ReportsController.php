<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Dompdf\Dompdf;
use Src\Models\Inventory;
use Src\Models\Sale;
use Src\Models\User;

class ReportsController extends Controller
{
    protected $redirect = "reports";
    protected $prefix = "reports";
    protected $required;
    protected $dompdf;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->required = ["name","price","stock","min_stock","description"];
        $this->dompdf = new Dompdf(["enable_remote" => true]);
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $this->template("admin_report");
    }

    public function sales()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["users"] = (new User())->all();

        $this->template("admin_report_sales", $data);
    }

    public function inventories()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $this->template("admin_report_invetory", $data);
    }

    public function sales_pdf()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $filters = [];

        $user_id = isset($request["user_id"]) ? $request["user_id"] : "";
        $start = isset($request["start"]) ? $request["start"] : "";
        $end = isset($request["end"]) ? $request["end"] : "";
        $status = isset($request["status"]) ? $request["status"] : "";
        $order = isset($request["order"]) ? $request["order"] : "desc";

        if (!empty($user_id)) {
            $filters["user_id"] = $user_id;
        }

        if (!empty($start)) {
            $filters["start"] = $start;
        }

        if (!empty($end)) {
            $filters["end"] = $end;
        }

        if (!empty($status)) {
            $filters["status"] = $status;
        }

        $filters["order"] = $order;

        $data = array();
        $data["list"] = (new Sale())->all($filters);
        $data["filters"] = $request;

        ob_start(); // armazena na memória, não imprime na tela
        $this->view("report_sales_pdf", $data);
        $html = ob_get_contents();
        ob_end_clean();
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper("A4", "landscape");
        $this->dompdf->render();
        $this->dompdf->stream("report_sales_pdf", ["Attachment" => false]);
    }

    public function invetories_pdf()
    {
        $data = array();
        $data["list"] = (new Inventory())->all(["min" => true]);

        ob_start(); // armazena na memória, não imprime na tela
        $this->view("report_inventories_pdf", $data);
        $html = ob_get_contents();
        ob_end_clean();
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper("A4", "landscape");
        $this->dompdf->render();
        $this->dompdf->stream("report_inventories_pdf", ["Attachment" => false]);
    }
}