<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;

class NotfoundController extends Controller
{
    public function __construct()
    {
        parent::__construct("template_admin");
    }

    public function index()
    {
        $this->template("404");
    }
}