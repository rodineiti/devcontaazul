<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Purchase;
use Src\Models\PurchasesProduct;

class PurchasesController extends Controller
{
    protected $model;
    protected $redirect = "purchases";
    protected $prefix = "purchases";
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->model = new Purchase();
        $this->required = ["name"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $limit = $this->limit ?? 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $results = $this->model->all([], $limit, $offset);
        $resultsCount = $this->model->count(["id"]);
        $pages = ceil($resultsCount / $limit);

        $data = array();
        $data["list"] = $results;
        $data["total"] = count($results);
        $data["pages"] = $pages;
        $data["page"] = $page;
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_purchase", $data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_purchase_create", $data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $model = $this->model->create($data);

        if (!$model) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        setFlashMessage("success", ["Item adicionado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $data = array();
        $data["model"] = $model;
        $data["purchaseProducts"] = (new PurchasesProduct())->getByPurchase($model->id);
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_purchase_edit", $data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->model->updateData($id, $data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        setFlashMessage("success", ["Item atualizado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->model->destroy($model->id);

        setFlashMessage("success", ["Item deletado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}