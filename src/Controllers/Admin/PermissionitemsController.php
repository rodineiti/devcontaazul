<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\PermissionGroupItem;
use Src\Models\PermissionItem;

class PermissionitemsController extends Controller
{
    protected $items;
    protected $redirect = "permissionitems";
    protected $prefix = "permission-items";
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->items = new PermissionItem();
        $this->required = ["name","slug"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["list"] = $this->items->all();
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_permission_item", $data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_permission_item_create", $data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $item = $this->items->create($data);

        if (!$item) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$item = $this->items->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $data = array();
        $data["itemPermission"] = $item;
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_permission_item_edit", $data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->items->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        if (empty($data["name"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->items->updateData($id, $data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$item = $this->items->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $itemCheck = (new PermissionGroupItem())->getByPermissionItem($id);

        // não permite deletar se houver items usuários neste grupo
        if (count($itemCheck)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->items->destroy($item->id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}