<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Inventory;
use Src\Models\Sale;
use Src\Models\SaleProduct;
use Src\Models\User;

class SalesController extends Controller
{
    protected $model;
    protected $redirect = "sales";
    protected $prefix = "sales";
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->model = new Sale();
        $this->required = ["user_id","product_id"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $limit = $this->limit ?? 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $results = $this->model->all([], $limit, $offset);
        $resultsCount = $this->model->count(["id"]);
        $pages = ceil($resultsCount / $limit);

        $data = array();
        $data["list"] = $results;
        $data["total"] = count($results);
        $data["pages"] = $pages;
        $data["page"] = $page;
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_sale", $data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["users"] = (new User())->all();
        $data["products"] = (new Inventory())->all();
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_sale_create", $data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $model = $this->model->create($data);

        if (!$model) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        setFlashMessage("success", ["Item adicionado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $data = array();
        $data["model"] = $model;
        $data["users"] = (new User())->all();
        $data["products"] = (new Inventory())->all();
        $data["saleProducts"] = (new SaleProduct())->getBySale($model->id);
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_sale_edit", $data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->model->updateData($id, $data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        setFlashMessage("success", ["Item atualizado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->model->destroy($model->id);

        setFlashMessage("success", ["Item deletado com sucesso"]);
        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}