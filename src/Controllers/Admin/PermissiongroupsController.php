<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\PermissionGroup;
use Src\Models\Admin as User;
use Src\Models\PermissionGroupItem;
use Src\Models\PermissionItem;

class PermissiongroupsController extends Controller
{
    protected $group;
    protected $redirect = "permissiongroups";
    protected $prefix = "permission-groups";
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->group = new PermissionGroup();
        $this->required = ["name"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["list"] = $this->group->all();
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $this->template("admin_permission_group", $data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $data["permissionItems"] = (new PermissionItem())->all();
        $this->template("admin_permission_group_create", $data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        if (isset($data["permissionItems"]) && !is_array($data["permissionItems"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $group = $this->group->create($data);

        if (!$group) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$group = $this->group->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $data = array();
        $data["group"] = $group;
        $data["prefix"] = $this->prefix;
        $data["redirect"] = $this->redirect;
        $data["permissionItems"] = (new PermissionItem())->all();
        $data["itemsByPermission"] = (new PermissionGroupItem())->getByPermission($group->id, $group->company_id);
        $this->template("admin_permission_group_edit", $data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->group->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        if (!$this->required($data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (isset($data["permissionItems"]) && !is_array($data["permissionItems"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->group->updateData($id, $data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            setFlashMessage("info", ["Você não tem permissão para realizar esta operação."]);
            header("Location: " . back());
            exit;
        }

        if (!$group = $this->group->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $usersGroup = (new User())->all(["permission_group_id" => $group->id]);

        // não permite deletar se houver items usuários neste grupo
        if (count($usersGroup)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->group->destroy($group->id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}