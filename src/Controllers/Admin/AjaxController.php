<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Inventory;
use Src\Models\User;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct("template_admin");
    }

    public function get_clients()
    {
        $this->auth("admins");

        if ($this->method() !== "GET") {
            $this->json(["error" => true, "message" => "Method not allowed."]);
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $searchTerm = (!empty($request["term"]) ? $request["term"] : null);
        if ($searchTerm) {
            $filters["searchTerm"] = $searchTerm;
        }

        $results = (new User())->all($filters, $this->limit ?? 10);

        foreach ($results as $result) {
            $result->url = BASE_URL . "admin/users/edit/{$result->id}";
        }

        $this->json(["error" => false, "data" => $results]);
    }

    public function get_inventories()
    {
        $this->auth("admins");

        if ($this->method() !== "GET") {
            $this->json(["error" => true, "message" => "Method not allowed."]);
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $searchTerm = (!empty($request["term"]) ? $request["term"] : null);
        if ($searchTerm) {
            $filters["searchTerm"] = $searchTerm;
        }

        $results = (new Inventory())->all($filters, $this->limit ?? 10);

        foreach ($results as $result) {
            $result->url = BASE_URL . "admin/inventories/edit/{$result->id}";
        }

        $this->json(["error" => false, "data" => $results]);
    }
}