<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Admin as User;
use Src\Models\Inventory;
use Src\Models\Purchase;
use Src\Models\Sale;
use Src\Models\User as Client;
use Src\Models\PermissionGroup;
use Src\Models\PermissionItem;

class AdminController extends Controller
{
    protected $user;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->user = new User();
    }

    public function index()
    {
        if (auth("admins")) {
            $this->home();
        } else {
            $this->template("admin_login");
        }
    }

    public function home()
    {
        $this->auth("admins");

        $data = array();
        $data["count_users"] = (new Client())->count(["id"]);
        $data["count_groups"] = (new PermissionGroup())->count(["id"]);
        $data["count_items"] = (new PermissionItem())->count(["id"]);
        $data["sum_sales"] = (new Sale())->sum(["start" => date("Y-m-d", strtotime("-30 days")), "end" => date("Y-m-d")]);
        $data["sum_purchases"] = (new Purchase())->sum(["start" => date("Y-m-d", strtotime("-30 days")), "end" => date("Y-m-d")]);
        $data["count_inventories"] = (new Inventory())->count(["id"]);
        $data["days_list"] = array();
        for ($i = 30; $i > 0; $i--) {
            $data["days_list"][] = date("d/m", strtotime("-{$i} days"));
        }
        $data["report_sales"] = (new Sale())->resumeGrafic(
            ["start" => date("Y-m-d", strtotime("-30 days")), "end" => date("Y-m-d")]
        );
        $data["report_purchages"] = (new Purchase())->resumeGrafic(
            ["start" => date("Y-m-d", strtotime("-30 days")), "end" => date("Y-m-d")]
        );
        $data["report_status"] = (new Sale())->resumeStatus(
            ["start" => date("Y-m-d", strtotime("-30 days")), "end" => date("Y-m-d")]
        );

        $this->template("admin_home", $data);
    }

    public function profile()
    {
        $this->template("admin_profile");
    }

    public function login()
    {
        if(isset($_POST["email"]) && !empty($_POST["email"])) {
            $email = addslashes($_POST["email"]);
            $password = $_POST["password"];

            $user = $this->user->attempt($email, $password);

            if (!$user) {
                header("Location: " . BASE_URL . "admin?login&error=true");
                exit;
            }

            $this->user->setSession($user);

            header("Location: " . BASE_URL . "admin/home");
            exit;
        }

        header("Location: " . BASE_URL . "admin?login&error=true");
        exit;
    }

    public function update()
    {
        if($_POST) {
            if (!$this->user->updateProfile(auth("admins")->id, $_POST)) {
                header("Location: " . BASE_URL . "admin/profile?error=fields");
                exit;
            } else {
                header("Location: " . BASE_URL . "admin/profile?success=true");
                exit;
            }
        }

        header("Location: " . BASE_URL . "admin/profile?error=fields");
        exit;
    }

    public function logout()
    {
        session_start();
        $this->user->destroySession();
        header("Location: " . BASE_URL . "admin?login");
    }
}