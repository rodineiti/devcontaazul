<?php

namespace Src\Controllers;

use Src\Core\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    	header("Location: " . BASE_URL . "admin");
    	exit;
        $data = array();
        $this->template("home", $data);
    }
}