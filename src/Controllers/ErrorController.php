<?php

namespace Src\Controllers;

use Src\Core\Controller;

class ErrorController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($id)
    {
        $data = array();
        $data["status"] = "danger";
        $data["error"] = is_numeric($id) ? getError($id) : $id;
        $this->template("error", $data);
    }
}