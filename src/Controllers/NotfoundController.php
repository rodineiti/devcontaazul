<?php

namespace Src\Controllers;

use Src\Core\Controller;

class NotfoundController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $this->template("404", $data);
    }
}