<?php

namespace Src\Support;

use NFePHP\Common\Files\FilesFolders;
use NFePHP\Extras\Danfe;
use NFePHP\Extras\NfephpException;
use NFePHP\NFe\MakeNFe;
use NFePHP\NFe\ToolsNFe;

class Nfe
{
    private $nfe;
    private $tools;

    public function __construct()
    {
        $this->nfe = new MakeNFe();
        $this->tools = new ToolsNFe("nfe/files/config.json");
    }

    public function issue($ID, $data, $products, $fatinfo)
    {
        //Dados da NFe - infNFe
        $cUF = $this->tools->aConfig['cUF']; //codigo numerico do estado
        $natOp = 'Venda de Produto'; //natureza da operação
        $indPag = '0'; //0=Pagamento à vista; 1=Pagamento a prazo; 2=Outros
        $mod = '55'; //modelo da NFe 55 ou 65 essa última NFCe
        $serie = '1'; //serie da NFe
        $nNF = $ID; // numero da NFe
        $dhEmi = date("Y-m-d\TH:i:sP"); // Data de emissão
        $dhSaiEnt = date("Y-m-d\TH:i:sP"); //Data de entrada/saida
        $tpNF = '1'; // 0=entrada; 1=saida
        $idDest = '1'; //1=Operação interna; 2=Operação interestadual; 3=Operação com exterior.
        $cMunFG = $this->tools->aConfig['cMun']; // Código do Município
        $tpImp = '1'; //0=Sem geração de DANFE; 1=DANFE normal, Retrato; 2=DANFE normal, Paisagem; 3=DANFE Simplificado; 4=DANFE NFC-e; 5=DANFE NFC-e em mensagem eletrônica
        $tpEmis = '1'; //1=Emissão normal (não em contingência);
        //2=Contingência FS-IA, com impressão do DANFE em formulário de segurança;
        //3=Contingência SCAN (Sistema de Contingência do Ambiente Nacional);
        //4=Contingência DPEC (Declaração Prévia da Emissão em Contingência);
        //5=Contingência FS-DA, com impressão do DANFE em formulário de segurança;
        //6=Contingência SVC-AN (SEFAZ Virtual de Contingência do AN);
        //7=Contingência SVC-RS (SEFAZ Virtual de Contingência do RS);
        //9=Contingência off-line da NFC-e (as demais opções de contingência são válidas também para a NFC-e);
        //Nota: Para a NFC-e somente estão disponíveis e são válidas as opções de contingência 5 e 9.
        $tpAmb = $this->tools->aConfig['tpAmb']; //1=Produção; 2=Homologação
        $finNFe = '1'; //1=NF-e normal; 2=NF-e complementar; 3=NF-e de ajuste; 4=Devolução/Retorno.
        $indFinal = '0'; //0=Normal; 1=Consumidor final;
        $indPres = '2'; //0=Não se aplica (por exemplo, Nota Fiscal complementar ou de ajuste);
        //1=Operação presencial;
        //2=Operação não presencial, pela Internet;
        //3=Operação não presencial, Teleatendimento;
        //4=NFC-e em operação com entrega a domicílio;
        //9=Operação não presencial, outros.
        $procEmi = '0'; //0=Emissão de NF-e com aplicativo do contribuinte;
        //1=Emissão de NF-e avulsa pelo Fisco;
        //2=Emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site do Fisco;
        //3=Emissão NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.
        $verProc = $this->tools->aConfig['vApp']; //versão do aplicativo emissor
        $dhCont = ''; //entrada em contingência AAAA-MM-DDThh:mm:ssTZD
        $xJust = ''; //Justificativa da entrada em contingência
        $cnpj = $this->tools->aConfig['cnpj']; // CNPJ do emitente

        //Numero e versão da NFe (infNFe)
        $ano = date('y', strtotime($dhEmi));
        $mes = date('m', strtotime($dhEmi));

        $chave = $this->nfe->montaChave($cUF, $ano, $mes, $cnpj, $mod, $serie, $nNF, $tpEmis, $nNF);
        $versao = $this->tools->aConfig['nfeVersao'];
        $resp = $this->nfe->taginfNFe($chave, $versao);

        $cDV = substr($chave, -1); //Digito Verificador da Chave de Acesso da NF-e, o DV é calculado com a aplicação do algoritmo módulo 11 (base 2,9) da Chave de Acesso.
        //tag IDE
        $resp = $this->nfe->tagide($cUF, $nNF, $natOp, $indPag, $mod, $serie, $nNF, $dhEmi, $dhSaiEnt, $tpNF, $idDest, $cMunFG, $tpImp, $tpEmis, $cDV, $tpAmb, $finNFe, $indFinal, $indPres, $procEmi, $verProc, $dhCont, $xJust);

        //Dados do emitente
        $CPF = ''; // Para Emitente CPF
        $xNome = $this->tools->aConfig['razaosocial'];
        $xFant = $this->tools->aConfig['nomefantasia'];
        $IE = $this->tools->aConfig['ie']; // Inscrição Estadual
        $IEST = $this->tools->aConfig['iest']; // IE do Substituti Tributário
        $IM = $this->tools->aConfig['im']; // Inscrição Municipal
        $CNAE = $this->tools->aConfig['cnae']; // CNAE Fiscal
        $CRT = $this->tools->aConfig['regime']; // CRT (Código de Regime Tributário), 1=simples nacional
        $resp = $this->nfe->tagemit($cnpj, $CPF, $xNome, $xFant, $IE, $IEST, $IM, $CNAE, $CRT);

        //endereço do emitente
        $xLgr = $this->tools->aConfig['xLgr'];
        $nro = $this->tools->aConfig['nro'];
        $xCpl = $this->tools->aConfig['xCpl'];
        $xBairro = $this->tools->aConfig['xBairro'];
        $cMun = $this->tools->aConfig['cMun'];
        $xMun = $this->tools->aConfig['xMun'];
        $UF = $this->tools->aConfig['UF'];
        $CEP = $this->tools->aConfig['CEP'];
        $cPais = $this->tools->aConfig['cPais'];
        $xPais = $this->tools->aConfig['xPais'];
        $fone = $this->tools->aConfig['fone'];
        $resp = $this->nfe->tagenderEmit($xLgr, $nro, $xCpl, $xBairro, $cMun, $xMun, $UF, $CEP, $cPais, $xPais, $fone);

        //destinatário
        $CNPJ = $data['cnpj'];
        $CPF = $data['cpf'];
        $idEstrangeiro = $data['idestrangeiro'];
        $xNome = $data['nome']; // Nome/Razão Social
        $email = $data['email'];
        $indIEDest = $data['iedest']; // Indica se tem IE (vazio ou 1)
        $IE = $data['ie']; // Insc. Estadual
        $ISUF = $data['isuf']; // Insc. SUFRAMA
        $IM = $data['im']; // Insc. Municipal
        $resp = $this->nfe->tagdest($CNPJ, $CPF, $idEstrangeiro, $xNome, $indIEDest, $IE, $ISUF, $IM, $email);

        //Endereço do destinatário
        $xLgr = $data['end']['logradouro'];
        $nro = $data['end']['numero'];
        $xCpl = $data['end']['complemento'];
        $xBairro = $data['end']['bairro'];
        $xMun = $data['end']['mu'];
        $UF = $data['end']['uf'];
        $CEP = $data['end']['cep'];
        $xPais = $data['end']['pais'];
        $fone = $data['end']['fone'];
        $cMun = $data['end']['cmu']; // Código do Municipio
        $cPais = $data['end']['cpais']; // Código do País
        $resp = $this->nfe->tagenderDest($xLgr, $nro, $xCpl, $xBairro, $cMun, $xMun, $UF, $CEP, $cPais, $xPais, $fone);

        // Inicialização de váriaveis
        $vBC = 0;
        $vICMSDeson = 0;
        $vProd = 0;
        $vFrete = 0;
        $vSeg = 0;
        $vDesc = 0;
        $vOutro = 0;
        $vII = 0;
        $vIPI = 0;
        $vIOF = 0;
        $vPIS = 0;
        $vCOFINS = 0;
        $vICMS = 0;
        $vBCST = 0;
        $vST = 0;
        $vISS = 0;

        $nItem = 1;
        foreach($products as $product) {

            $cProd = $product['cProd']; // Código do Produto
            $cEAN = $product['cEAN']; // Código de Barras (EAN)
            $xProd = $product['xProd']; // Descrição do Produto
            $NCM = $product['NCM']; // Código NCM (Nomenclatura Comum do MERCOSUL)
            $EXTIPI = $product['EXTIPI']; // Código de excessão do NCM
            $CFOP = $product['CFOP']; // Código Fiscal de Operações e Prestações
            $uCom = $product['uCom']; // Unidade Comercial do produto
            $qCom = $product['qCom']; // Quantidade
            $vUnCom = $product['vUnCom']; // Valor Unitário
            $vProd += $product['vProd']; // Valor do Produto
            $cEANTrib = $product['cEANTrib']; // Código de Barra Tributável
            $uTrib = $product['uTrib']; // Unidade Tributável
            $qTrib = $product['qTrib']; // Quantidade Tributável
            $vUnTrib = $product['vUnTrib']; // Valor Unitário de tributação
            $vFrete += $product['vFrete']; // Valor Total do Frete
            $vSeg += $product['vSeg']; // Valor Total do Seguro
            $vDesc += $product['vDesc']; // Valor do Desconto
            $vOutro += $product['vOutro']; // Outras Despesas
            $indTot = $product['indTot']; // Indica se valor do Item (vProd) entra no valor total da NF-e. As vezes é um brinde
            $xPed = $product['xPed']; // Número do Pedido de Compra
            $nItemPed = $product['nItemPed']; // Item do Pedido de Compra
            $nFCI = $product['nFCI']; // Número de controle da FCI - Importação
            $vBC += $product['bc']; // Base de cálculo

            // Adiciona o produto na nota
            $this->nfe->tagprod($nItem, $cProd, $cEAN, $xProd, $NCM, $EXTIPI, $CFOP, $uCom, $qCom, $vUnCom, $product['vProd'], $cEANTrib, $uTrib, $qTrib, $vUnTrib, $product['vFrete'], $product['vSeg'], $product['vDesc'], $product['vOutro'], $indTot, $xPed, $nItemPed, $nFCI);

            // Imposto Total deste produto
            $vTotTrib = $product['impostoTotal']; // ICMS + IPI + PIS + COFINS, etc...
            $this->nfe->tagimposto($nItem, $vTotTrib);

            // ICMS
            $vICMS += $product['icms'];
            //$this->>nfe->tagICMS(...);

            // IPI
            $vIPI += $product['ipi'];
            //$this->>nfe->tagIPI(...);

            // PIS
            $vPIS += $product['pis'];
            //$this->>nfe->tagPIS(...);

            // CONFINS
            $vCOFINS += $product['cofins'];
            //$this->>nfe->tagCOFINS(...);

            $nItem++;
        }

        // Valor da NF
        $vNF = number_format($vProd-$vDesc-$vICMSDeson+$vST+$vFrete+$vSeg+$vOutro+$vII+$vIPI, 2, '.', '');

        // Valor Total Tributável
        $vTotTrib = number_format($vICMS+$vST+$vII+$vIPI+$vPIS+$vCOFINS+$vIOF+$vISS, 2, '.', '');

        // Grupos Totais
        $this->nfe->tagICMSTot($vBC, $vICMS, $vICMSDeson, $vBCST, $vST, $vProd, $vFrete, $vSeg, $vDesc, $vII, $vIPI, $vPIS, $vCOFINS, $vOutro, $vNF, $vTotTrib);

        // Frete
        $modFrete = '9'; //0=Por conta do emitente; 1=Por conta do destinatário/remetente; 2=Por conta de terceiros; 9=Sem Frete;
        $this->nfe->tagtransp($modFrete);

        // Dados da fatura
        $nFat = $fatinfo['nfat']; // Número da Fatura
        $vOrig = $fatinfo['vorig']; // Valor original da fatura
        $vDesc = $fatinfo['vdesc']; // Valor do desconto
        $vLiq = $fatinfo['nfat']; // Valor Líquido
        $this->nfe->tagfat($nFat, $vOrig, $vDesc, $vLiq);

        // Monta a NF-e e retorna o resultado
        $resp = $this->nfe->montaNFe();
        if($resp === true) {
            $xml = $this->nfe->getXML();

            // Assina o XML
            $xml = $this->tools->assina($xml);

            // Valida o XML
            $v = $this->tools->validarXml($xml);

            if($v == false) {
                foreach($this->tools->errors ?? [] as $erro) {
                    if(is_array($erro)) {
                        foreach($erro as $er) {
                            echo $er."<br/>";
                        }
                    } else {
                        echo $erro."<br/>";
                    }
                }

                exit;
            }

            $idLote = '';
            $indSinc = '0'; // 0=asíncrono, 1=síncrono
            $flagZip = false;
            $resposta = array();

            // Envia para o SEFAZ
            $this->tools->sefazEnviaLote((array)$xml, $tpAmb, $idLote, $resposta, $indSinc, $flagZip);

            // Consulta o RECIBO
            $protXML = $this->tools->sefazConsultaRecibo($resposta['nRec'], $tpAmb);

            // Chave aleatória para o XML/PDF
            $chave = md5(time().rand(0,9999));
            $xmlName = $chave.'.xml';
            $danfeName = $chave.'.pdf';

            // Salva os arquivos temporário e validado
            $pathNFefile = "nfe/files/nfe/validadas/".$xmlName;
            $pathProtfile = "nfe/files/nfe/temp/".$xmlName;
            $pathDanfeFile = "nfe/files/nfe/danfe/".$danfeName;
            file_put_contents($pathNFefile, $xml);
            file_put_contents($pathProtfile, $protXML);

            // Adiciona o Protocolo
            $this->tools->addProtocolo($pathNFefile, $pathProtfile, true);

            // Gera o DANFE
            $docxml = FilesFolders::readFile($pathProtfile);

			$docFormat = $this->tools->aConfig['aDocFormat']->format;
			$docPaper = $this->tools->aConfig['aDocFormat']->paper;
			$docLogo = $this->tools->aConfig['aDocFormat']->pathLogoFile;

			try {
                $danfe = new Danfe($docxml, $docFormat, $docPaper, $docLogo);
                $danfe->montaDANFE();
                $danfe->printDANFE($pathDanfeFile, "F");

                return $chave;
            } catch (NfephpException $exception) {
			    die($exception->getMessage());
            }
		} else {
            foreach($this->nfe->erros as $erro) {
                echo $erro['tag'].' - '.$erro['desc']."<br/>";
            }
            return null;
        }
    }
}