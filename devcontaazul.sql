-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: devcontaazul
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permission_group_id` int(11) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `name_email_FULLTEXT` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,1,1,'admin','admin@admin.com','$2y$10$JxsGcZCAPf6pl8BvnppKtONxdVxkJW1pKjWzlr67oSBFS5/nD/oVm','2020-08-04 22:03:21','2020-08-05 17:22:19');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `nfe_number` int(8) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Companhia RdnContaAzul',10000008,'2020-08-04 22:03:21','2020-08-11 20:44:29');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float DEFAULT '0',
  `stock` int(11) NOT NULL COMMENT 'quantidade em estoque',
  `min_stock` int(11) NOT NULL COMMENT 'quantidade minima em estoque',
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_description_FULLTEXT` (`name`,`description`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventories`
--

LOCK TABLES `inventories` WRITE;
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
INSERT INTO `inventories` VALUES (3,1,'Produto 1',100,1,10,'asdfsadf','2020-08-10 22:11:09','2020-08-11 18:44:43'),(4,1,'Produto 2',500,13,10,'asdjfudedsofajkdslfads','2020-08-10 22:11:21','2020-08-11 18:43:32');
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_history`
--

DROP TABLE IF EXISTS `inventory_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventory_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL COMMENT 'quem fez a operação',
  `action_type` char(5) NOT NULL COMMENT 'in => entrada, out => saida',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_history`
--

LOCK TABLES `inventory_history` WRITE;
/*!40000 ALTER TABLE `inventory_history` DISABLE KEYS */;
INSERT INTO `inventory_history` VALUES (1,1,2,1,'add','2020-08-10 20:31:03',NULL),(2,1,2,1,'edit','2020-08-10 20:31:13',NULL),(3,1,2,1,'del','2020-08-10 20:31:20',NULL),(4,1,3,1,'add','2020-08-10 22:11:09',NULL),(5,1,4,1,'add','2020-08-10 22:11:21',NULL),(6,1,3,1,'down','2020-08-10 23:22:56',NULL),(7,1,3,1,'down','2020-08-10 23:33:43',NULL),(8,1,3,1,'down','2020-08-10 23:35:35',NULL),(9,1,4,1,'edit','2020-08-11 16:20:01',NULL),(10,1,4,1,'edit','2020-08-11 16:20:59',NULL),(11,1,3,1,'down','2020-08-11 18:42:52',NULL),(12,1,4,1,'down','2020-08-11 18:43:32',NULL),(13,1,3,1,'down','2020-08-11 18:44:43',NULL);
/*!40000 ALTER TABLE `inventory_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_group_items`
--

DROP TABLE IF EXISTS `permission_group_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_group_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `permission_group_id` int(11) NOT NULL,
  `permission_item_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_group_items`
--

LOCK TABLES `permission_group_items` WRITE;
/*!40000 ALTER TABLE `permission_group_items` DISABLE KEYS */;
INSERT INTO `permission_group_items` VALUES (94,1,1,1,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(95,1,1,2,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(96,1,1,3,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(97,1,1,4,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(98,1,1,5,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(99,1,1,6,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(100,1,1,7,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(101,1,1,8,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(102,1,1,9,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(103,1,1,10,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(104,1,1,11,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(105,1,1,13,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(106,1,1,14,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(107,1,1,15,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(108,1,1,16,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(109,1,1,17,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(110,1,1,18,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(111,1,1,19,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(112,1,1,20,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(113,1,1,21,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(114,1,1,22,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(115,1,1,23,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(116,1,1,24,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(117,1,1,25,'2020-08-11 16:53:08','2020-08-11 16:53:08'),(118,1,1,26,'2020-08-11 16:53:08','2020-08-11 16:53:08');
/*!40000 ALTER TABLE `permission_group_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
INSERT INTO `permission_groups` VALUES (1,1,'Super Administrador','2020-08-05 16:32:32','2020-08-05 16:32:32');
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_items`
--

DROP TABLE IF EXISTS `permission_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_items`
--

LOCK TABLES `permission_items` WRITE;
/*!40000 ALTER TABLE `permission_items` DISABLE KEYS */;
INSERT INTO `permission_items` VALUES (1,1,'Listar Clientes','users-index','2020-08-05 21:05:20','2020-08-05 22:42:20'),(2,1,'Criar Clientes','users-create','2020-08-05 21:05:20','2020-08-05 21:05:20'),(3,1,'Editar Clientes','users-edit','2020-08-05 21:05:20','2020-08-05 21:05:20'),(4,1,'Deletar Clientes','users-destroy','2020-08-05 21:05:20','2020-08-05 21:05:20'),(5,1,'Listar Grupo de Permissões','permission-groups-index','2020-08-05 21:05:20','2020-08-05 21:05:20'),(6,1,'Criar Grupo de Permissões','permission-groups-create','2020-08-05 21:05:20','2020-08-05 21:05:20'),(7,1,'Editar Grupo de Permissões','permission-groups-edit','2020-08-05 21:05:20','2020-08-05 21:05:20'),(8,1,'Deletar Grupo de Permissões','permission-groups-destroy','2020-08-05 21:05:20','2020-08-05 21:05:20'),(9,1,'Listar Item de Permissões','permission-items-index','2020-08-05 22:36:33','2020-08-05 22:36:33'),(10,1,'Criar Item de Permissões','permission-items-create','2020-08-05 22:36:33','2020-08-05 22:36:33'),(11,1,'Editar Item de Permissões','permission-items-edit','2020-08-05 22:36:33','2020-08-05 22:36:33'),(13,1,'Deletar Item de Permissões','permission-items-destroy','2020-08-05 22:46:17','2020-08-05 22:46:17'),(14,1,'Listar inventário','inventories-index','2020-08-10 20:03:39','2020-08-10 20:03:39'),(15,1,'Editar Inventário','inventories-edit','2020-08-10 20:03:48','2020-08-10 20:03:48'),(16,1,'Criar Inventário','inventories-create','2020-08-10 20:03:59','2020-08-10 20:09:40'),(17,1,'Deletar Inventário','inventories-destroy','2020-08-10 20:04:15','2020-08-10 20:04:15'),(18,1,'Listar vendas','sales-index','2020-08-10 20:41:06','2020-08-10 20:41:06'),(19,1,'Criar venda','sales-create','2020-08-10 20:41:14','2020-08-10 20:41:14'),(20,1,'Editar venda','sales-edit','2020-08-10 20:41:24','2020-08-10 20:41:24'),(21,1,'Deletar venda','sales-destroy','2020-08-10 20:41:32','2020-08-10 20:41:32'),(22,1,'Relatórios','reports-index','2020-08-11 14:28:19','2020-08-11 14:28:19'),(23,1,'Listar Compras','purchases-index','2020-08-11 16:52:38','2020-08-11 16:52:38'),(24,1,'Criar Compras','purchases-create','2020-08-11 16:52:46','2020-08-11 16:52:46'),(25,1,'Editar Compras','purchases-edit','2020-08-11 16:52:53','2020-08-11 16:52:53'),(26,1,'Deletar Compras','purchases-destroy','2020-08-11 16:53:00','2020-08-11 16:53:00');
/*!40000 ALTER TABLE `permission_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchases` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL COMMENT 'quem fez a operação',
  `amount` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
INSERT INTO `purchases` VALUES (2,1,1,100,1,'2020-08-10 17:19:11','2020-08-11 18:33:29'),(3,1,1,250,1,'2020-08-10 17:19:22','2020-08-11 18:33:29');
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases_products`
--

DROP TABLE IF EXISTS `purchases_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchases_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `purchase_price` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases_products`
--

LOCK TABLES `purchases_products` WRITE;
/*!40000 ALTER TABLE `purchases_products` DISABLE KEYS */;
INSERT INTO `purchases_products` VALUES (11,1,2,'produto 1',1,100,'2020-08-11 17:19:11',NULL),(13,1,3,'produto 1',5,50,'2020-08-11 17:19:32',NULL);
/*!40000 ALTER TABLE `purchases_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL COMMENT 'quem fez a operação',
  `user_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `key_nfe` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (17,1,1,1,1385,1,NULL,'2020-08-10 23:35:35','2020-08-11 17:19:39'),(18,1,1,1,500,0,NULL,'2020-08-10 18:42:52','2020-08-11 18:43:12'),(19,1,1,2,20,2,NULL,'2020-08-10 18:43:32','2020-08-11 18:43:40'),(20,1,1,1,1,1,NULL,'2020-08-10 18:44:43','2020-08-11 18:45:04');
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_products`
--

DROP TABLE IF EXISTS `sales_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sales_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `sale_price` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_products`
--

LOCK TABLES `sales_products` WRITE;
/*!40000 ALTER TABLE `sales_products` DISABLE KEYS */;
INSERT INTO `sales_products` VALUES (65,1,17,3,1,1385,'2020-08-11 17:19:39',NULL),(66,1,18,3,1,500,'2020-08-11 18:42:52',NULL),(67,1,19,4,2,10,'2020-08-11 18:43:32',NULL),(68,1,20,3,1,1,'2020-08-11 18:44:43',NULL);
/*!40000 ALTER TABLE `sales_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `address` varchar(150) NOT NULL,
  `number` varchar(50) NOT NULL,
  `district` varchar(150) NOT NULL,
  `city` varchar(150) NOT NULL,
  `state` char(2) NOT NULL,
  `complement` varchar(150) DEFAULT NULL,
  `country` varchar(150) NOT NULL,
  `observation` text,
  `stars` int(11) DEFAULT '3',
  `cpf` varchar(20) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `foreignId` varchar(20) DEFAULT NULL,
  `iedest` varchar(45) DEFAULT NULL,
  `ie` varchar(150) DEFAULT NULL,
  `isuframa` varchar(45) DEFAULT NULL,
  `im` varchar(45) DEFAULT NULL,
  `city_code` varchar(45) DEFAULT NULL,
  `contry_code` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `name_email_FULLTEXT` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Cliente 1','cliente1@teste.com','','1141111111','05338100','Rua eulo maroniss','101','jaguaré','são paulo','SP','ap','Brasil',NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-10 18:29:50','2020-08-10 18:39:27'),(2,1,'Cliente 2','cliente2@teste.com','$2y$10$6v1Qbt6Ox1xilGjYKg1M4uznMFYlsQb/abyJZ2GlOJVr0r/SOfoq.','41111111','06665022','Estrada do Sapiantã','1111111','Conjunto Habitacional - Setor A','Itapevi','sp','','Brasil',NULL,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-10 18:47:32',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-11 17:59:39
