$(function () {
    $("#cep").blur(function() {
        var cep = $(this).val().replace(/\D/g, '');
        if (cep !== "") {
            var isValidCep = /^[0-9]{8}$/;
            if(isValidCep.test(cep)) {
                preInputs();
                // inicio uma requisição
                $.ajax({
                    type: "GET",
                    url : "https://viacep.com.br/ws/"+ cep +"/json/?callback=?",
                    dataType : "json",
                    success : function(dados){
                        console.log(dados);
                        if (dados.erro){
                            clearInputs();
                        } else {
                            setData(dados);
                        }
                    },
                    beforeSend: function(){
                        preInputs();
                    },
                });//termina o ajax
            }
            else {
                clearInputs();
            }
        }
        else {
            clearInputs();
        }
    });

    function clearInputs() {
        $("#address").val("");
        $("#complement").val("");
        $("#district").val("");
        $("#city").val("");
        $("#state").val("");
    }

    function preInputs() {
        $("#address").val("...");
        $("#complement").val("...");
        $("#district").val("...");
        $("#city").val("...");
        $("#state").val("...");
    }

    function setData(data) {
        $("#address").val(data.logradouro);
        $("#complement").val(data.complement);
        $("#district").val(data.bairro);
        $("#city").val(data.localidade);
        $("#state").val(data.uf.toLowerCase()).change();
        $("#cep").prop("readonly", false);
    }

    $("#search").on("blur", function (e) {
        setTimeout(function () {
            $(".searchResults").hide();
        }, 500);
    });

    $("#search").on("keyup", function (e) {
        var dataAction = $(this).attr("data-action");
        var term = $(this).val();

        if (dataAction !== "") {
            $.ajax({
                url: baseUrl + "admin/ajax/" + dataAction,
                type:"GET",
                data:{term:term},
                dataType: "json",
                success: function (response) {
                    if ($(".searchResults").length === 0) {
                        $("#search").after("<div class='searchResults'></div>");
                    }

                    var html = "<div class=\"list-group\">";

                    for (var i in response.data) {
                        html += "<a class=\"list-group-item list-group-item-action\" href='"+response.data[i].url+"'>"+response.data[i].name+"</a>";
                    }

                    html += "</div>";

                    $(".searchResults").html(html);
                    $(".searchResults").show();
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    });

    $("#price").mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
    $("#amount").mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
    $(".sale_price").mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
    $(".purchase_price").mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });

    // clone element
    $(document).on("click", ".add-age-range", function(event) {
        $(".age-range-container").append($(".age-range-inputs").eq(0).clone().show());
        $(".sale_price").mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
        $(".purchase_price").mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
    });
    // remove element by index
    $(document).on("click", ".remove-age-range", function(event) {
        var length = $(".remove-age-range").length;
        var index = $(".remove-age-range").index(this);
        if (length > 1) {
            $(".age-range-inputs").eq(index).remove();
        }
    });

    $("form#form-report-sales").on("submit", function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        window.open(baseUrl + "/admin/reports/sales_pdf?" + data, "report-sale", "width=700,height=500");
        return false;
    });

    $("form#form-report-inventories").on("submit", function (e) {
        e.preventDefault();
        window.open(baseUrl + "/admin/reports/invetories_pdf", "report-inventory", "width=700,height=500");
        return false;
    });
});