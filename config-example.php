<?php
ini_set("display_errors", 1);
ini_set("error_reporting", E_ALL);
ini_set('xdebug.overload_var_dump', 1);

require "env.php";

$config = array();

if (ENV === "development") {
    $config["dbname"] = "devcontaazul";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";
} else {
    $config["dbname"] = "devcontaazul";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";
}

define("CONF_DEFAULT_LANG", "pt-br");
define("BASE_URL", "http://localhost");
define("SITE_NAME", "SITENAME");

/**
 * PASSWORD
 */
define("CONF_PASSWORD_ALGO", PASSWORD_DEFAULT);
define("CONF_PASSWORD_OPTION", ["cost" => 10]);
define("CONF_JWT_SECRET", "123456879");
define("CONF_CONTACT_PHONE", "(11) 9999-9999");
define("CONF_CONTACT_EMAIL", "contact@contact.com");
define("CONF_CONTACT_ADDRESS", "Address");
define("CONF_CEP_ORIGEM", "00000000");
define("CONF_LIMIT_CEP_SHIPPING", 200);
define("CONF_LIMIT_CEP_DIAMETER", 91);
define("CONF_LIMIT_CEP_LENGTH", 40);

/**
 * PAYMENT CONFIG
 */
define("CONF_SITENAME_CHECKOUT", SITE_NAME);
define("CONF_VERSION_CHECKOUT", "1.0.0");
define("CONFIG_PAGSEGURO_ENVIRONMENT", "sandbox");
define("CONF_EMAIL_PAGSEGURO", "");
define("CONF_TOKEN_PAGSEGURO", "");
define("CONF_CHARSET_PAGSEGURO", "UTF-8");
define("CONF_FILE_LOG_PAGSEGURO", "pagseguro.log");

define("CONFIG_MERCADO_PAGO_SANDBOX", true);
define("CONFIG_MERCADO_PAGO_ENVIRONMENT", "sandbox_init_point");
define("CONF_APIKEY_MERCADO_PAGO", "");
define("CONF_SECRETKEY_MERCADO_PAGO", "");

define("CONFIG_PAYPAL_ENVIRONMENT", "sandbox");
define("CONF_CLIENTID_PAYPAL", "");
define("CONF_SECRETKEY_PAYPAL", "");

define("CONF_CLIENTID_GERENCIANET", "");
define("CONF_SECRETKEY_GERENCIANET", "");
define("CONF_SECRETKEY_GERENCIANET_SANDBOX", true);

global $db;

try {
    $db = new PDO("mysql:dbname={$config["dbname"]};host={$config["dbhost"]}", $config["dbuser"], $config["dbpass"],
        [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ]);
} catch (PDOException $exception) {
    echo "ERROR DATABASE: " . $exception->getMessage();
    exit;
}