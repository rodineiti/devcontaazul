# devcontaazul

Clone the repository

    git clone git@gitlab.com:rodineiti/devcontaazul.git

Switch to the repo folder

    cd devcontaazul
    cp config-example.php config.php
    
Edit file config.php, and set connection mysql

    $config["dbname"] = "devcontaazul";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";

Dump file devcontaazul.sql into database

Run server php or your server (Wamp, Mamp, Xamp), and open in the browser localhost:2020
  
    php -S localhost:2020

Url ADMIN:

    Admin login: http://localhost:2020/devcontaazul/admin?login

    login: admin@admin.com
    password: 123456

Prints:

Dashboard Admin

![image](https://user-images.githubusercontent.com/25492122/90024318-1b037e00-dc8b-11ea-9826-53cfebba5852.png)


Add Sale

![image](https://user-images.githubusercontent.com/25492122/90024393-31113e80-dc8b-11ea-8d62-a86e6c22e8e1.png)